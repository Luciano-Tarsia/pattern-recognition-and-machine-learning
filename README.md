**Programa**

*Introducción:*

- Aplicaciones
- Ajuste polinomial de curvas
- Repaso de Conceptos Básicos de Teorı́a de la Probabilidad
- Selección de Modelos
- Teorı́a de la decisión
- Funciones de pérdida para regresión
- Multiplicadores de Lagrange

*Modelos lineales para regresión:*

- Funciones base para modelos lineales
- Máxima verosimilitud y cuadrados mı́nimos
- Geometrı́a de cuadrados mı́nimos
- Aprendizaje secuencial
- Cuadrados mı́nimos con regularización
- Salidas múltiples
- Descomposición sesgo-varianza

*Modelos lineales para clasificación:*

- Caso de dos clases
- Caso de múltiples clases
- Clasificador cuadrático
- Discriminante de Fisher
- Perceptron
- Regresión Logı́stica
- Caso de 2 clases
- Caso de más de 2 clases

*Expectation Maximization:*

- Mixtura de Gaussianas
- K-means
- Codificación 1-de-K
- Variables “latentes”
- EM para Mixtura de Gaussianas
- EM en general
- ¿Porqué EM converge?

*Supprort Vector Machines SVM:*

- Formulación del problema
- Problema dual de Lagrange
- Dualidad débil
- Dualidad fuerte y condiciones de Slater
- Condiciones KKT (Karush-Kuhn-Tucker)
- Dualidad fuerte en el caso SVM
- Caso de clases no linealmente separables: Kernels
- Caso de más de dos clases: 1-contra-1, 1-contra-todos, hinge loss.

*El problema del aprendizaje:*

- Formulación del problema
- Types of Learning
- Cuándo es posible el aprendizaje?
- Error y Ruido

*Training versus Testing:*

- Teoría de la Generalización – Dimensión de VC
- Cota de la generalización
- The Test Set
- Balance Aproximación-Generalización

**Bibliografía:**

    [1] Pattern Recognition and Machine Learning by C. Bishop, Springer 2006. 

    [2] The Elements of Statistical Learning, T. Hastie et al, Springer, 2008.

    [3] Learning from Data, Yasser S. Abu-Mostafa, 2012.

    [4] Pattern Classification (2nd. Edition) by R. O. Duda, P. E. Hart and D. Stork, Wiley     2002.

    [5] Pattern Recognition by S. Theodoridis and K. Koutroumbas 2009, Elsevier Inc.

    [6] Convex Optimization, by Lieven Vandenberghe and Stephen P. Boyd, 2004.