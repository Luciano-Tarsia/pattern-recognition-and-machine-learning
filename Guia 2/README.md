Para esta guía se requirió que hagamos solo 2 de los 4 puntos del enunciado.

Junto a mi compañero hicimos los puntos 1 (Clasificador por cuadrados mínimos), 3 (Regresión logística) y parte del 4 (implementamos y pusimos a prueba k-means). Además hicimos algunos análisis adicionales de casos que nos parecían interesantes.

También escribimos todo el código correspondeinte a EM para mixtura de gaussianas, pero por problemas de redondeos no funcionó correctamente.